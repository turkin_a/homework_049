var line = '\n';

for (var i = 0; i < 10; i++) {
    for (var j = 0; j < 10; j++) {
	if (i === 0 || i === 9) {
	    line += '--';
	    continue;
	}

	if (j === 0) {
	    line += '| ';
	    continue;
	}

	if (j === 9) {
	    line += ' |';
	    continue;
	}

	if ((i + j) %2 === 0) {
	    line += '  ';
	} else {
	    line += '██';
	}
    }
    line += '\n';
}

console.log(line);